using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class HealthDecay: MonoBehaviour
    {
        public float DecayPerSec;
        
        private EntityData _entityData;
        
        private void Awake()
        {
            _entityData = GetComponent<EntityData>();
        }

        private void Start()
        {
            Observable.EveryUpdate()
                .Subscribe(_ => { _entityData.CurrentHealth.Value -= Time.deltaTime * DecayPerSec; })
                .AddTo(this);
        }
    }
}