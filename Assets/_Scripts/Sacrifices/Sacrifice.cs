using UniRx;
using UnityEngine;

namespace _Scripts.Sacrifices
{
    [CreateAssetMenu(menuName = "Game/Sacrifice")]
    public class Sacrifice: ScriptableObject
    {
        public string Name;
        public string Description;
        public Sprite Icon;
        
        public BoolReactiveProperty IsActive = new BoolReactiveProperty(false);

        private void OnEnable()
        {
            IsActive.Value = false;
        }
    }
}