using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.UI
{
    public class PlayerHealthBar: MonoBehaviour
    {
        public EntityData Entity;
        
        public Slider HealthSlider;

        private Tween _lastValueChangeTween;
        
        private void Start()
        {
            Entity.CurrentHealth
                .WithLatestFrom(Entity.MaxHealth, Tuple.Create)
                .Subscribe(tuple =>
                {
                    if (_lastValueChangeTween != null)
                    {
                        _lastValueChangeTween.Kill();
                        _lastValueChangeTween = null;
                    }
                    var healthPercentage = tuple.Item1 / tuple.Item2;

                    _lastValueChangeTween = HealthSlider.DOValue(healthPercentage, 0.3f).SetEase(Ease.OutExpo);
                })
                .AddTo(this);
        }
    }
}