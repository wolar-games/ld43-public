using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using _Scripts.Sacrifices;

namespace _Scripts.UI
{
    public class SacrificeButton: MonoBehaviour
    {
        public Image Image;
        public TextMeshProUGUI Title;
        public TextMeshProUGUI Description;
        public Button Button;

        public IObservable<Sacrifice> Load(Sacrifice sacrifice)
        {
            Image.sprite = sacrifice.Icon;
            Title.text = sacrifice.Name;
            Description.text = sacrifice.Description;
            return Button.OnClickAsObservable().Select(_ => sacrifice);
        }
    }
}