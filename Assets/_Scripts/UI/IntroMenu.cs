using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts.UI
{
    public class IntroMenu: MonoBehaviour
    {
        private bool _loading = false;

        public TextMeshProUGUI Instruction;
        
        private void Update()
        {
            if (!_loading && Input.GetKeyDown(KeyCode.Space))
            {
                _loading = true;
                Instruction.text = "Loading";
                SceneManager.LoadScene(1);
            }
        }
    }
}