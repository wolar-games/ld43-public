using System.Collections.Generic;
using UniRx;
using UnityEngine;
using _Scripts.Sacrifices;

namespace _Scripts.UI
{
    public class VisionBlocking: MonoBehaviour
    {
        public PlayerData PlayerData;

        public GameObject Left;
        public GameObject Right;
        public GameObject Top;
        public GameObject Bottom;
        public GameObject Middle;

        private void Start()
        {
            var pairs = new List<Tuple<Sacrifice, GameObject>>()
            {
                new Tuple<Sacrifice, GameObject>(PlayerData.LeftVision, Left),
                new Tuple<Sacrifice, GameObject>(PlayerData.RightVision, Right),
                new Tuple<Sacrifice, GameObject>(PlayerData.MiddleVision, Middle),
                new Tuple<Sacrifice, GameObject>(PlayerData.BottomVision, Bottom),
                new Tuple<Sacrifice, GameObject>(PlayerData.TopVision, Top)
            };

            foreach (var pair in pairs)
            {
                var capturedObject = pair.Item2;
                pair.Item1.IsActive
                    .Subscribe(a =>
                    {
                        capturedObject.SetActive(a);
                    })
                    .AddTo(this);
            }
        }
    }
}