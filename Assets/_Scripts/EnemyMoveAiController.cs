﻿using UniRx;
using UnityEngine;
using UnityEngine.AI;
using Vector3 = UnityEngine.Vector3;

public class EnemyMoveAiController : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;

    private NavMeshSurface _navMeshSurface;

    private GameObject _playerGameObject;

    private MoveableEnemyData _moveableEnemyData;

    private Vector3 _originPosition;

    private bool _playerSpotted;

    private void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _moveableEnemyData = GetComponent<MoveableEnemyData>();

        _navMeshSurface = GameObject.FindGameObjectWithTag("NavMesh").GetComponent<NavMeshSurface>();
        _playerGameObject = GameObject.FindGameObjectWithTag("Player");

        // keep original position for some type of behaviors
        _originPosition = transform.position;
    }

    private void Start()
    {
        Observable.EveryFixedUpdate()
            .TakeUntilDisable(this)
            .Subscribe(_ =>
            {
                var distanceFromPlayer = Vector3.Distance(_playerGameObject.transform.position, transform.position);
                if (distanceFromPlayer < _moveableEnemyData.PlayerDetectionDistance || (_moveableEnemyData.MovementBehavior == EnemyMovementBehavior.HuntUntilDeath && _playerSpotted))
                {
                    _playerSpotted = true;

                    switch (_moveableEnemyData.ChasingBehavior)
                    {
                        case EnemyChasingBehavior.Dodge:
                            _navMeshAgent.SetDestination(_playerGameObject.transform.position);
                            break;
                        case EnemyChasingBehavior.KeepDistanceRandom:
                            break;
                        case EnemyChasingBehavior.KeepDistanceCircling:
                            if (distanceFromPlayer <= _moveableEnemyData.PlayerKeepDistance)
                            {
                                if (!IsMovingPending())
                                {
                                    var nextAngle = GetAngleAgainstPlayer() - 90 + 10;
                                    var circleDistance = GetUnitOnCircle(nextAngle, _moveableEnemyData.PlayerKeepDistance - 0.1f);
                                    var nextPoint = new Vector3(_playerGameObject.transform.position.x - circleDistance.x, _playerGameObject.transform.position.y, _playerGameObject.transform.position.z + circleDistance.y);
                                    _navMeshAgent.SetDestination(nextPoint);
                                }
                            }
                            else
                            {
                                var nextPoint = _playerGameObject.transform.position - ((_playerGameObject.transform.position - transform.position).normalized * _moveableEnemyData.PlayerKeepDistance);
                                _navMeshAgent.SetDestination(nextPoint);
                            }
                            break;
                    }
                }
                else if (!IsMovingPending())
                {
                    switch (_moveableEnemyData.MovementBehavior)
                    {
                        case EnemyMovementBehavior.Random:
                            _navMeshAgent.SetDestination(GenerateRandomDestOnMap());
                            break;
                        case EnemyMovementBehavior.BackToOrigin:
                            _navMeshAgent.SetDestination(_originPosition);
                            break;
                        case EnemyMovementBehavior.Stationary:
                            // stay on last position
                            break;
                    }
                }
            })
            .AddTo(this);
    }

    private Vector3 GenerateRandomDestOnMap()
    {
        var radius = 50;

        var randomDirection = Random.insideUnitSphere * radius;

        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

        return hit.position;
    }
    
    private bool IsMovingPending()
    {
        if (!_navMeshAgent.pathPending)
        {
            if (_navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance)
            {
                if (!_navMeshAgent.hasPath || _navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    return false;
                }
            }
        }

        return true;
    }

    private float GetAngleAgainstPlayer()
    {
        var lookPos = _playerGameObject.transform.position - transform.position;
        return Quaternion.LookRotation(lookPos).eulerAngles.y;
    }

    private Vector2 GetUnitOnCircle(float angleDegrees, float radius)
    {
        var angleRadians = angleDegrees * Mathf.PI / 180.0f;
        var x = radius * Mathf.Cos(angleRadians);
        var y = radius * Mathf.Sin(angleRadians);
        return new Vector2(x, y);
    }
}
