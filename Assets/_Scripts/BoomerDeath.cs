using Cinemachine;
using Lean.Pool;
using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class BoomerDeath: MonoBehaviour
    {
        public EntityData EntityData;

        [Space]
        public AudioSource AudioSourcePrefab;
        public ParticleSystem ParticleSystemPrefab;
        public CinemachineImpulseSource ImpulseSource;

        private void Start()
        {
            EntityData.CurrentHealth
                .Select(h => h <= 0)
                .Where(isDead => isDead)
                .DistinctUntilChanged()
                .Subscribe(_ =>
                {
                    var audio = LeanPool.Spawn(AudioSourcePrefab, transform.position, Quaternion.identity);
                    audio.Play();
                    LeanPool.Despawn(audio, 2f);

                    var particles = LeanPool.Spawn(ParticleSystemPrefab, transform.position, Quaternion.identity);
                    particles.Play();
                    LeanPool.Despawn(particles, 2f);
                    
                    ImpulseSource.GenerateImpulse();
                })
                .AddTo(this);
        }
    }
}