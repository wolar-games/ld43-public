using Cinemachine;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.PostProcessing;
using WolarGames.Variables;

namespace _Scripts
{
    // Handling input
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController: MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private PlayerData _playerData;
        private EntityData _entityData;
        public WeaponSystem PrimaryWeapon;
        public WeaponSystem SecondaryWeapon;

        public PostProcessVolume PostProcessVolume;
        private ChromaticAberration _chromaticAberration;
        private Tween _postProcessTween;

        public CinemachineImpulseSource ShotFiredSource;
        public CinemachineImpulseSource DamageTakenSource;

        [Header("Fire:")]
        public float MainGunReloadTime = 1f;
        private float _mainGunReloadRemaining = 0f;

        public float SecondaryGunWeaponSpeed = 0.025f;
        private float _secondaryGunReloadRemaining = 0f;
        
        private void Awake()
        {
            _chromaticAberration = PostProcessVolume.profile.GetSetting<ChromaticAberration>();
            _entityData = GetComponent<EntityData>();
            _playerData = GetComponent<PlayerData>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            Cursor.visible = false;
            
            Observable.EveryUpdate()
                .Where(_ => Input.GetMouseButton(0))
                .Where(_ => _mainGunReloadRemaining <= 0f)
                .Where(_ => !EventSystem.current.IsPointerOverGameObject())
                .Where(_ => !_playerData.FirePrimarySacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    _mainGunReloadRemaining = MainGunReloadTime;
                    ShotFiredSource.GenerateImpulse();
                    PrimaryWeapon.Fire();
                })
                .AddTo(this);
            
            Observable.EveryUpdate()
                .Where(_ => Input.GetMouseButton(1))
                .Where(_ => _secondaryGunReloadRemaining <= 0f)
                .Where(_ => !_playerData.FireSecondarySacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    _secondaryGunReloadRemaining = SecondaryGunWeaponSpeed;
                    SecondaryWeapon.Fire();
                })
                .AddTo(this);

            Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    _mainGunReloadRemaining -= Time.deltaTime; 
                    _secondaryGunReloadRemaining -= Time.deltaTime;
                })
                .AddTo(this);
            
            Observable.EveryFixedUpdate()
                .Where(_ => Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                .Where(_ => !_playerData.GoFrontSacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
                    Vector3 movement = transform.forward * _playerData.MaxSpeed * Time.deltaTime;
                    movement.y = 0;
                    _playerData.CurrentMovement.ConstantValue = movement.normalized * _playerData.MaxSpeed.Value;

                    // Apply this movement to the rigidbody's position.
                    _rigidbody.MovePosition(_rigidbody.position + movement);
                })
                .AddTo(this);
            
            Observable.EveryFixedUpdate()
                .Where(_ => Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                .Where(_ => !_playerData.GoBackSacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
                    Vector3 movement = transform.forward * -_playerData.MaxSpeed * Time.deltaTime;
                    movement.y = 0;
                    _playerData.CurrentMovement.ConstantValue = movement.normalized * _playerData.MaxSpeed.Value;

                    // Apply this movement to the rigidbody's position.
                    _rigidbody.MovePosition(_rigidbody.position + movement);
                })
                .AddTo(this);

            Observable.EveryFixedUpdate()
                .Where(_ => !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.UpArrow))
                .Subscribe(_ => { _playerData.CurrentMovement.ConstantValue = Vector3.zero; })
                .AddTo(this);
            
            Observable.EveryFixedUpdate()
                .Where(_ => Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                .Where(_ => !_playerData.TurnLeftSacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    float turn = -_playerData.MaxRotationSpeed * Time.deltaTime;

                    // Make this into a rotation in the y axis.
                    Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);

                    // Apply this rotation to the rigidbody's rotation.
                    _rigidbody.MoveRotation (_rigidbody.rotation * turnRotation);
                })
                .AddTo(this);
            
            Observable.EveryFixedUpdate()
                .Where(_ => Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                .Where(_ => !_playerData.TurnRightSacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    float turn = _playerData.MaxRotationSpeed * Time.deltaTime;

                    // Make this into a rotation in the y axis.
                    Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);

                    // Apply this rotation to the rigidbody's rotation.
                    _rigidbody.MoveRotation (_rigidbody.rotation * turnRotation);
                })
                .AddTo(this);

            _entityData.OnDamageTaken
                .Where(damage => damage > 1.2f)
                .Subscribe(_ =>
                {
                    if (_postProcessTween != null)
                    {
                        _postProcessTween.Kill();
                        _postProcessTween = null;
                    }

                    var currentValue = _chromaticAberration.intensity.value;
                    currentValue += 0.6f;
                    currentValue = Mathf.Clamp01(currentValue);

                    Time.timeScale = 0.35f;
                    _postProcessTween = DOTween.Sequence()
                        .Append(DOTween.To(() => currentValue, x => _chromaticAberration.intensity.Override(x), 0f, 0.6f).SetEase(Ease.OutQuad))
                        .Join(DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1f, 0.1f).SetEase(Ease.InCubic)).SetUpdate(true);
                    
                    DamageTakenSource.GenerateImpulse();
                })
                .AddTo(this);
        }
    }
}