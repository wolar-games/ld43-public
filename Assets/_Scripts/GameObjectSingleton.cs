using System.Collections.Generic;
using UnityEngine;

namespace _Scripts
{
    public class GameObjectSingleton: MonoBehaviour
    {
        private static Dictionary<string, GameObjectSingleton> _singletons = new Dictionary<string, GameObjectSingleton>();
        
        private void Awake()
        {
            if (_singletons.ContainsKey(name))
            {
                if (_singletons[name] != this)
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                _singletons[name] = this;
                DontDestroyOnLoad(gameObject);
            }    
        }

        private void Update()
        {
            var modifier =
                Input.GetKey(KeyCode.LeftControl) ||
                Input.GetKey(KeyCode.RightControl) ||
                Input.GetKey(KeyCode.LeftCommand) ||
                Input.GetKey(KeyCode.RightCommand);
            if (modifier && Input.GetKey(KeyCode.Q))
            {
                Application.Quit();
            }
        }
    }
}