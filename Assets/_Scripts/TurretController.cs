﻿using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class TurretController : MonoBehaviour
    {
        public Transform TurretTransform;

        public Transform TargetTransform;

        public AudioSource AudioSource;

        private PlayerData _playerData;

        private void Awake()
        {
            _playerData = GetComponent<PlayerData>();
        }

        private void Start()
        {
            if (AudioSource != null)
            {
                AudioSource.Play();
                AudioSource.Pause();
            }

            Observable.EveryUpdate()
                .Where(_ => !_playerData.RotateTurretSacrifice.IsActive.Value)
                .Subscribe(_ =>
                {
                    var lookPos = TargetTransform.position - TurretTransform.position;
                    lookPos.y = 0;

                    var rotation = Quaternion.LookRotation(lookPos);
                    var angleDiff = (rotation.eulerAngles.y - TurretTransform.rotation.eulerAngles.y);
                    if (Mathf.Abs(angleDiff) > 180)
                    {
                        angleDiff = -angleDiff;
                    }

                    var angleSpeedReduced = Mathf.Sign(angleDiff) * Mathf.Min(Mathf.Abs(angleDiff), _playerData.MaxTurretRotationSpeed * Time.deltaTime);

                    if (AudioSource != null)
                    {
                        if (30 * Time.deltaTime < Mathf.Abs(angleSpeedReduced))
                        {
                            if (!AudioSource.isPlaying)
                            {
                                AudioSource.UnPause();    
                            }
                        }
                        else
                        {
                            AudioSource.Pause();
                        }
                    }
                    
                    TurretTransform.rotation = Quaternion.Euler(0, TurretTransform.rotation.eulerAngles.y + angleSpeedReduced, 0);
                })
                .AddTo(this);
        }
    }
}
