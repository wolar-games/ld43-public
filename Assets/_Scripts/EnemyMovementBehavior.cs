﻿public enum EnemyMovementBehavior
{
    Random,
    Stationary,
    BackToOrigin,
    HuntUntilDeath
}
