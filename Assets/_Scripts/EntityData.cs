﻿using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class EntityData : MonoBehaviour
    {
        public FloatReactiveProperty MaxHealth; 
        
        
        [Header("Runtime:")]
        public FloatReactiveProperty CurrentHealth;

        public Subject<float> OnDamageTaken = new Subject<float>();

        private void Awake()
        {
            CurrentHealth.Value = MaxHealth.Value;
        }
    }
}
