using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using _Scripts.Sacrifices;
using _Scripts.UI;

namespace _Scripts
{
    public class SacrificeController: MonoBehaviour
    {
        public List<Sacrifice> Sacrifices;
        public EntityData Player;

        public RectTransform Buttons;
        public CanvasGroup ButtonsCanvasGroup;
        public TextMeshProUGUI Title;
        public SacrificeButton LeftButton;
        public SacrificeButton MiddleButton;
        public SacrificeButton RightButton;

        [Header("Give up:")]
        public Button GiveUpButton;
        public CanvasGroup GiveUpButtonCanvasGroup;
        public GoalCheck GoalCheck;
        
        private CompositeDisposable _compositeDisposable = new CompositeDisposable();
        
        private void Awake()
        {
            foreach (var sacrifice in Sacrifices)
            {
                sacrifice.IsActive.Value = false;
            }
        }

        private void Start()
        {
            _compositeDisposable.AddTo(this);
            
            Buttons.gameObject.SetActive(false);
            GiveUpButtonCanvasGroup.interactable = false;
            GiveUpButtonCanvasGroup.alpha = 0;
            
            Player.CurrentHealth
                .Select(health => health <= 0)
                .DistinctUntilChanged()
                .Where(dead => dead)
                .Subscribe(_ =>
                {
                    DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 0, 1.5f).SetEase(Ease.OutCubic).SetUpdate(true);
                    PresentSacrifices();
                })
                .AddTo(this);
            
            GiveUpButton.OnClickAsObservable().Buffer(GiveUpButton.OnClickAsObservable().Throttle(TimeSpan.FromMilliseconds(250), Scheduler.MainThreadIgnoreTimeScale))
                .Where(xs => xs.Count >= 2)
                .Subscribe(xs =>
                {
                    GiveUpButton.gameObject.SetActive(false);
                    GoalCheck.DisplayMessage(GoalCheck.FailedMessage); 
                });
        }

        private void PresentSacrifices()
        {
            _compositeDisposable.Clear();

            var numberOfSacrifices = Sacrifices.Where(s => s.IsActive.Value).Count();
            var validSacrifices = Sacrifices.Where(s => !s.IsActive.Value).OrderBy(a => Guid.NewGuid()).Take(3).ToList();

            if (validSacrifices.Count == 0)
            {
                GoalCheck.DisplayMessage(GoalCheck.FailedMessage);
                return;
            }
            
            GiveUpButton.gameObject.SetActive(numberOfSacrifices >= 2);
            GiveUpButtonCanvasGroup.alpha = 0;
            
            if (validSacrifices.Count > 0)
            {
                LeftButton.gameObject.SetActive(true);
                LeftButton.Load(validSacrifices[0])
                    .Subscribe(sacrifice =>
                    {
                        sacrifice.IsActive.Value = true; 
                        ContinueGame();
                    })
                    .AddTo(_compositeDisposable);
            }
            else
            {
                LeftButton.gameObject.SetActive(false);
            }
            
            if (validSacrifices.Count > 1)
            {
                MiddleButton.gameObject.SetActive(true);
                MiddleButton.Load(validSacrifices[1])
                    .Subscribe(sacrifice =>
                    {
                        sacrifice.IsActive.Value = true;
                        ContinueGame();
                    })
                    .AddTo(_compositeDisposable);
            }
            else
            {
                MiddleButton.gameObject.SetActive(false);
            }
            
            if (validSacrifices.Count > 2)
            {
                RightButton.gameObject.SetActive(true);
                RightButton.Load(validSacrifices[2])
                    .Subscribe(sacrifice =>
                    {
                        sacrifice.IsActive.Value = true;
                        ContinueGame();
                    })
                    .AddTo(_compositeDisposable);
            }
            else
            {
                RightButton.gameObject.SetActive(false);
            }
            
            Buttons.localScale = Vector3.zero;
            ButtonsCanvasGroup.interactable = false; 
            Buttons.gameObject.SetActive(true);
            var tween = Buttons.DOScale(1, 0.6f).SetEase(Ease.OutBack).SetUpdate(true);
            GiveUpButtonCanvasGroup.DOFade(1, 0.6f).SetUpdate(true);
            tween.onComplete = () =>
            {
                Cursor.visible = true;
                GiveUpButtonCanvasGroup.interactable = true;
                ButtonsCanvasGroup.interactable = true; 
            };
        }

        private void ContinueGame()
        {
            Cursor.visible = false;
            ButtonsCanvasGroup.interactable = false;
            GiveUpButtonCanvasGroup.interactable = false;
            DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1, 1.5f).SetEase(Ease.InCubic).SetUpdate(true);
            Buttons.DOScale(0, 0.4f).SetEase(Ease.OutCubic).SetUpdate(true);
            GiveUpButtonCanvasGroup.DOFade(0, 0.4f).SetUpdate(true);
            Player.CurrentHealth.Value = Player.MaxHealth.Value;
        }
    }
}