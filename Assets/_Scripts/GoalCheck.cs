using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

namespace _Scripts
{
    public class GoalCheck: MonoBehaviour
    {
        public GameObject[] ToDisable;
        public CanvasGroup SuccessMessage;
        public CanvasGroup FailedMessage;
        
        public GameObject Player;

        public PostProcessVolume PostProcess;

        public TextMeshProUGUI SacrificesCountText;
        public SacrificeController SacrificeController;

        private bool _finished;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == Player)
            {
                var entityData = other.GetComponent<EntityData>();
                entityData.CurrentHealth.Value = 200;

                var decay = other.GetComponent<HealthDecay>();
                decay.DecayPerSec = 0;
                
                DisplayMessage(SuccessMessage);
            }    
        }

        private void Update()
        {
            if (_finished && Input.GetKeyDown(KeyCode.Space))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(1);
            }
        }

        public void SetSacrificesCount(int count)
        {
            SacrificesCountText.text = string.Format("You have made {0} sacrifices", count);
        }

        public void DisplayMessage(CanvasGroup message)
        {
            SetSacrificesCount(SacrificeController.Sacrifices.Count(s => s.IsActive.Value));   
            _finished = true;
            message.alpha = 0;
            message.gameObject.SetActive(true);

            Time.timeScale = 0;
            var tween = message.DOFade(1, 0.3f).SetUpdate(true);
            tween.onComplete = () =>
            {
                foreach (var toDisable in ToDisable)
                {
                    toDisable.SetActive(false);
                }
                
                PostProcess.profile.GetSetting<ChromaticAberration>().enabled.Override(false);
                PostProcess.profile.GetSetting<Vignette>().enabled.Override(false);
                
                Player.gameObject.SetActive(false);
            };
        }
    }
}