﻿using UnityEngine;
using WolarGames.Variables;

public class MoveableEnemyData : MonoBehaviour
{
    public EnemyMovementBehavior MovementBehavior;

    public EnemyChasingBehavior ChasingBehavior;

    public FloatReference PlayerDetectionDistance;

    public FloatReference PlayerKeepDistance;

    public bool GizmosVisible = true;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (GizmosVisible)
        {
            Gizmos.DrawWireSphere(transform.position, PlayerDetectionDistance);
        }
    }
}
