﻿using UnityEngine;
using UniRx;
using _Scripts;

public class CollisionExplosionController : MonoBehaviour
{
    public float CollisionDamage;

    private EntityData _entityData;

    void Awake()
    {
        _entityData = GetComponent<EntityData>();
    }

    void OnCollisionEnter(Collision collision)
    {
        var bomber = collision.gameObject.FindParentWithTag("Bomber");
        if (bomber != null)
        {
            var bomberData = collision.gameObject.GetComponentInParent<EntityData>();
            if (bomberData != null && bomberData.CurrentHealth.Value > 0)
            {
                // destroy enemy
                bomberData.CurrentHealth.Value = 0;
                bomberData.OnDamageTaken.OnNext(100);

                // harm player
                _entityData.CurrentHealth.Value -= CollisionDamage;
                _entityData.OnDamageTaken.OnNext(CollisionDamage);
            }
        }
    }
}
