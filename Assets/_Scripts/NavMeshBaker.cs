using System;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace _Scripts
{
    public class NavMeshBaker: MonoBehaviour
    {
        private NavMeshSurface _surface;
        
        private void Awake()
        {
            _surface = GetComponent<NavMeshSurface>();
        }

        private void Start()
        {
            MessageBroker.Default.Receive<BakeMeshEvent>()
                .Buffer(TimeSpan.FromSeconds(5f))
                .Where(b => b.Count > 0)
                .Delay(TimeSpan.FromSeconds(1f))
                .Subscribe(_ => { _surface.BuildNavMesh(); })
                .AddTo(this);
        }
    }
}