using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class DestroyableObstacle: MonoBehaviour
    {
        private EntityData _entityData;
        
        private void Awake()
        {
            _entityData = GetComponent<EntityData>();
        }

        private void Start()
        {
            _entityData.CurrentHealth
                .Select(h => h <= 0)
                .Where(isDead => isDead)
                .Subscribe(_ =>
                {
                    MessageBroker.Default.Publish(new BakeMeshEvent());
                })
                .AddTo(this);
        }
    }
}