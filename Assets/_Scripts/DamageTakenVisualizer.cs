using System.Linq;
using Cinemachine;
using DG.Tweening;
using Lean.Pool;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;

namespace _Scripts
{
    public class DamageTakenVisualizer: MonoBehaviour
    {
        public bool KillOnZero = true;
        public ParticleSystem OnKillParticles;
        public DamageTakenSingleVisualizer[] Visualizers;
        public AudioSource DeathAudioSourcePrefab;
        
        private EntityData _entityData;
        private CinemachineImpulseSource _impulseSource;

        private void Awake()
        {
            _entityData = GetComponent<EntityData>();
            _impulseSource = GetComponent<CinemachineImpulseSource>();
            Assert.IsNotNull(_impulseSource, string.Format("Missing input source: {0}", name));
        }

        private void Start()
        {
            _entityData.OnDamageTaken
                .Subscribe(_ =>
                {
                    foreach (var visualizer in Visualizers)
                    {
                        visualizer.Visualize();
                    }
                })
                .AddTo(this);

            if (KillOnZero)
            {
                _entityData.CurrentHealth
                    .Select(h => h <= 0)
                    .Where(isDead => isDead)
                    .DistinctUntilChanged()
                    .Subscribe(_ =>
                    {
                        foreach (var visualizer in Visualizers)
                        {
                            visualizer.Visualize(1f);
                        }

                        var seq = DOTween.Sequence()
                            .Append(transform.DOScale(1.0125f, 0.2f).SetEase(Ease.OutCubic))
                            .Append(transform.DOScale(0f, 0.35f).SetEase(Ease.OutCubic))
                            .Insert(0, transform.DORotate(new Vector3(Random.Range(-360f, 360f),Random.Range(-360f, 360f),Random.Range(-360f, 360f)), 0.55f));

                        var deathAudioSource = LeanPool.Spawn(DeathAudioSourcePrefab, transform.position, Quaternion.identity);
                        if (deathAudioSource != null)
                        {
                            deathAudioSource.Play();
                        }
                        
                        seq.onComplete = () =>
                        {
                            var particleSystem = LeanPool.Spawn(OnKillParticles, transform.position, Quaternion.identity);

                            var main = particleSystem.main;
                            main.startColor = Visualizers.First().DamageTakenMaterial.color;
                            particleSystem.Stop();
                            particleSystem.Play();
                            
                            _impulseSource.GenerateImpulse();
                            LeanPool.Despawn(deathAudioSource, 1f);
                            LeanPool.Despawn(particleSystem, 1f);
                            
                            Destroy(gameObject);
                        };
                    })
                    .AddTo(this);
            }   
        }
    }
}