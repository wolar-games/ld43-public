using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using UnityEngine;

namespace _Scripts
{
    public class WeaponSystem: MonoBehaviour
    {
        public GameObject Projectile;
        public float Spread = 0f;
        public List<Transform> FireOrigins;
        
        public Transform CanonTransform;
        public ParticleSystem[] ShotFiredParticles;
        public AudioSource ShotFiredAudio;

        private Projectile _projectileData;
        private EntityData _entityData;

        private void Awake()
        {
            _entityData = GetComponentInParent<EntityData>();
            _projectileData = Projectile.GetComponent<Projectile>();
        }

        public void Fire()
        {
            foreach (var fireOrigin in FireOrigins)
            {
                var rotation = fireOrigin.rotation.eulerAngles;
                rotation.y += Random.Range(-Spread, Spread);
                var projectile = LeanPool.Spawn(Projectile, fireOrigin.position, Quaternion.Euler(rotation));
                projectile.GetComponent<Projectile>().Origin = _entityData;
            }

            if (CanonTransform != null)
            {
                DOTween.Sequence()
                    .Append(CanonTransform.DOBlendableLocalMoveBy(Vector3.back * 0.2f, 0.2f).SetEase(Ease.OutBack))
                    .Append(CanonTransform.DOBlendableLocalMoveBy(Vector3.forward * 0.2f, 0.2f)
                        .SetEase(Ease.OutBack));
            }

            foreach (var shotFiredParticle in ShotFiredParticles)
            {
                shotFiredParticle.Play();
            }

            if (ShotFiredAudio != null)
            {
                ShotFiredAudio.Play();
            }
        }

        public bool IsEnemyInSight()
        {
            foreach (var fireOrigin in FireOrigins)
            {
                var raycastOrigin = fireOrigin.position;
                raycastOrigin.y = 0.3f;

                RaycastHit hit;
                if (Physics.Raycast(raycastOrigin, fireOrigin.rotation * Vector3.forward, out hit,
                    _projectileData.MaxDistance, _projectileData.CollisionMask))
                {
                    return hit.collider.gameObject.FindParentWithTag("Enemy");
                }
            }

            return false;
        }
    }
}