﻿using UnityEngine;
using WolarGames.Variables;

public class TowerData : MonoBehaviour
{
    public FloatReference MaxTurretRotationSpeed;

    public FloatReference MaxFireDistance;

    public FloatReference MaxAimingDistance;

    public FloatReference FireInterval;

    public bool GizmosVisible = true;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if (GizmosVisible)
        {
            Gizmos.DrawWireSphere(transform.position, MaxFireDistance.Value);    
        }
    }
}
