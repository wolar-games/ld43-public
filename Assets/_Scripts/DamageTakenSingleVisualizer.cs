using System;
using DG.Tweening;
using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class DamageTakenSingleVisualizer: MonoBehaviour
    {
        public Material DamageTakenMaterial;

        public Renderer Renderer;
        private Material _originalMaterial;

        private IDisposable _lastDisposable;
        
        private void Awake()
        {
            Renderer = GetComponent<Renderer>();
            _originalMaterial = Renderer.material;
        }

        public void Visualize(float duration = 0.1f)
        {
            if (_lastDisposable != null)
            {
                _lastDisposable.Dispose();
                _lastDisposable = null;
            }

            Renderer.material = DamageTakenMaterial;
            _lastDisposable =
                Observable.Timer(TimeSpan.FromSeconds(duration))
                    .Subscribe(_ => { Renderer.material = _originalMaterial; });

            _lastDisposable.AddTo(this);
        }
    }
}