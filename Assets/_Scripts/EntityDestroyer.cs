using UniRx;
using UnityEngine;

namespace _Scripts
{
    [RequireComponent(typeof(EntityData))]
    public class EntityDestroyer: MonoBehaviour
    {
        private EntityData _entityData;

        private void Awake()
        {
            _entityData = GetComponent<EntityData>();
        }

        private void Start()
        {
            _entityData.CurrentHealth
                .Where(health => health <= 0)
                .Subscribe(_ =>
                {
                    //gameObject.SetActive(false);
                })
                .AddTo(this);
        }
    }
}