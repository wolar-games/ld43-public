using UnityEngine;

namespace _Scripts
{
    public class FollowMouse: MonoBehaviour
    {
        private Camera _main;
        
        private void Awake()
        {
            _main = Camera.main;
        }

        private void Update()
        {
            RaycastHit hit;
            var ray = _main.ScreenPointToRay(Input.mousePosition);
        
            if (Physics.Raycast(ray, out hit))
            {
                transform.position = hit.point;
            }
        }
    }
}