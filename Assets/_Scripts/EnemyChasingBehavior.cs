﻿public enum EnemyChasingBehavior
{
    Dodge,
    KeepDistanceRandom,
    KeepDistanceCircling
}
