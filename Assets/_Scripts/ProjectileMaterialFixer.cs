using UnityEngine;

namespace _Scripts
{
    public class ProjectileMaterialFixer: MonoBehaviour
    {
        private Renderer _renderer;
        public Material OriginalMaterial;
        
        private void Awake()
        {
            _renderer = GetComponentInChildren<Renderer>();
        }

        private void OnEnable()
        {
            if (_renderer != null && OriginalMaterial != null)
            {
                _renderer.material = OriginalMaterial;    
            }
        }
    }
}