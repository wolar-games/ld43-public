﻿using System;
using UniRx;
using UnityEngine;
using _Scripts;
using Random = System.Random;

public class TowerAIFireTurretController : MonoBehaviour
{
    public Transform TurretTransform;

    private GameObject _playerGameObject;

    private WeaponSystem _weaponSystem;

    private Projectile _projectile;

    private PlayerData _playerData;

    private TowerData _towerData;

    private float _WatchingAngle;

    private Random _random;

    private DateTime _lastFireDateTime = DateTime.UtcNow;

    private void Awake()
    {
        _playerGameObject = GameObject.FindGameObjectWithTag("Player");
        _playerData = _playerGameObject.GetComponent<PlayerData>();

        _weaponSystem = GetComponentInChildren<WeaponSystem>();
        _projectile = _weaponSystem.Projectile.GetComponent<Projectile>();

        _towerData = GetComponent<TowerData>();
        _random = new Random();
    }

    void Start()
    {
        Observable.Interval(TimeSpan.FromSeconds(2))
            .TakeUntilDisable(this)
            .Subscribe(_ =>
            {
                _WatchingAngle = _random.Next(0, 360);
            })
            .AddTo(this);

        Observable.EveryFixedUpdate()
            .TakeUntilDisable(this)
            .Subscribe(_ =>
            {
                var distanceFromPlayer = Vector3.Distance(GetPlayerFuturePosition(), TurretTransform.position);
                if (distanceFromPlayer < _towerData.MaxAimingDistance)
                {
                    SetAngleToPlayer();

                    if (distanceFromPlayer < _towerData.MaxFireDistance)
                    {
                        TryFireToPlayer();
                    }
                }
                else
                {
                    SetWatchAngle();
                }
            })
            .AddTo(this);
    }

    private void SetWatchAngle()
    {
        SetTurretAngle(_WatchingAngle);
    }

    private void SetAngleToPlayer()
    {
        var playerFuturePosition = GetPlayerFuturePosition();
        var lookPos = playerFuturePosition - TurretTransform.position;

        playerFuturePosition.y = 0;

        var rotation = Quaternion.LookRotation(lookPos);
        SetTurretAngle(rotation.eulerAngles.y);
    }

    private void SetTurretAngle(float angle)
    {
        var angleDiff = (angle - TurretTransform.rotation.eulerAngles.y);
        if (Mathf.Abs(angleDiff) > 180)
        {
            angleDiff = -angleDiff;
        }

        var angleSpeedReduced = Mathf.Sign(angleDiff) * Mathf.Min(Mathf.Abs(angleDiff), _towerData.MaxTurretRotationSpeed * Time.deltaTime);

        TurretTransform.rotation = Quaternion.Euler(0, TurretTransform.rotation.eulerAngles.y + angleSpeedReduced, 0);
    }

    private Vector3 GetPlayerFuturePosition()
    {
        var timeForProjectileTravel = Vector3.Distance(_playerGameObject.transform.position, TurretTransform.position) / _projectile.Speed;
        return _playerGameObject.transform.position + (_playerData.CurrentMovement.Value * timeForProjectileTravel);
    }

    private void TryFireToPlayer()
    {
        var turretPlayerDiffAngle = GetTurretPlayerAngleDiff();

        if (Math.Abs(turretPlayerDiffAngle) < 20)
        {
            var intervalFromLastFire = DateTime.UtcNow - _lastFireDateTime;

            if (intervalFromLastFire.TotalSeconds > _towerData.FireInterval && !_weaponSystem.IsEnemyInSight())
            {
                _weaponSystem.Fire();
                _lastFireDateTime = DateTime.UtcNow;
            }
        }
    }

    private float GetTurretPlayerAngleDiff()
    {
        var lookPos = GetPlayerFuturePosition() - TurretTransform.position;
        var angle = Quaternion.LookRotation(lookPos).eulerAngles.y;

        var angleDiff = (angle - TurretTransform.rotation.eulerAngles.y);
        if (Mathf.Abs(angleDiff) > 180)
        {
            angleDiff = -angleDiff;
        }

        return angleDiff;
    }
}
