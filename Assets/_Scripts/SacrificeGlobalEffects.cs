using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using WolarGames.Variables;

namespace _Scripts
{
    public class SacrificeGlobalEffects: MonoBehaviour
    {
        public PlayerData Player;

        [Header("HealthBar:")] 
        public GameObject HealthBar;

        [Header("Invisible Enemies:")] 
        public BoolVariable EnemiesVisible;
        
        [Header("Monochromatic:")]
        [ColorUsageAttribute(true,true)]
        public Color MonoChromaticColor;

        [Header("PostProcess:")] 
        public PostProcessVolume PostProcess;

        public Material[] Materials;

        private Dictionary<Material, Color> _originalColors = new Dictionary<Material, Color>();
        
        private void OnEnable()
        {
            foreach (var material in Materials)
            {
                _originalColors[material] = material.color;
            }
        }

        private void OnDisable()
        {
            foreach (var material in Materials)
            {
                material.color = _originalColors[material];
            }
        }

        private void Start()
        {
            EnemiesVisible.CurrentValue = true;
            PostProcess.profile.GetSetting<ChromaticAberration>().active = false;
            PostProcess.profile.GetSetting<Vignette>().active = false;
            
            Player.SeeColorsSacrifice.IsActive
                .Where(a => a)
                .Subscribe(_ =>
                {
                    foreach (var material in Materials)
                    {
                        material.color = MonoChromaticColor;
                    }
                })
                .AddTo(this);
            
            Player.HealthBarSacrifice.IsActive
                .Where(a => a)
                .Subscribe(_ =>
                {
                    HealthBar.SetActive(false);
                })
                .AddTo(this);
            
            Player.InvisibleEnemiesSacrifice.IsActive
                .Where(a => a)
                .Subscribe(_ =>
                {
                    EnemiesVisible.CurrentValue = false;
                })
                .AddTo(this);
            
            Player.GlitchScreenSacrifice.IsActive
                .Where(a => a)
                .Subscribe(_ =>
                {
                    PostProcess.profile.GetSetting<ChromaticAberration>().active = true;
                })
                .AddTo(this);
            
            Player.VisionSacrifice.IsActive
                .Where(a => a)
                .Subscribe(_ =>
                {
                    PostProcess.profile.GetSetting<Vignette>().active = true;
                })
                .AddTo(this);
        }
    }
}