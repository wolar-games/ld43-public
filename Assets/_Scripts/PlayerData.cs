using UnityEngine;
using WolarGames.Variables;
using _Scripts.Sacrifices;

namespace _Scripts
{
    public class PlayerData: MonoBehaviour
    {
        public FloatReference MaxSpeed;

        [Header("Runtime:")]
        public Vector3Reference CurrentMovement;
        
        public FloatReference MaxRotationSpeed;
        
        public FloatReference MaxTurretRotationSpeed;

        [Header("Sacrifices - Movement")] 
        public Sacrifice TurnLeftSacrifice;
        public Sacrifice TurnRightSacrifice;
        public Sacrifice GoFrontSacrifice;
        public Sacrifice GoBackSacrifice;
        
        [Header("Sacrifices - Meta")]
        public Sacrifice SeeColorsSacrifice;
        public Sacrifice HealthBarSacrifice;
        public Sacrifice InvisibleEnemiesSacrifice;
        public Sacrifice GlitchScreenSacrifice;
        public Sacrifice VisionSacrifice;
        public Sacrifice AudioSacrifice;
        
        [Header("Sacrifices - Weapons")]
        public Sacrifice RotateTurretSacrifice;
        public Sacrifice FirePrimarySacrifice;
        public Sacrifice FireSecondarySacrifice;

        [Header("Sacrifices - Vision")]
        public Sacrifice MiddleVision;
        public Sacrifice LeftVision;
        public Sacrifice RightVision;
        public Sacrifice TopVision;
        public Sacrifice BottomVision;
    }
}