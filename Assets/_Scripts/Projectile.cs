using Lean.Pool;
using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class Projectile: MonoBehaviour
    {
        public float Speed = 10f;
        public float Damage = 15f;
        public float MaxDistance = 200f;

        public GameObject OnCollisionParticlesPrefab;
        public LayerMask CollisionMask;

        public AudioSource CollisionAudioSource;
        public EntityData Origin;

        private float _distanceTraveled = 0f;
        
        private void OnEnable()
        {
            _distanceTraveled = 0;
        }

        private void Update()
        {
            RaycastHit hit;
            var distance = Speed * Time.deltaTime;

            var raycastOrigin = transform.position;
            raycastOrigin.y = 0.3f;
            if (Physics.Raycast(raycastOrigin, transform.rotation * Vector3.forward, out hit, distance, CollisionMask))
            {
                var data = hit.collider.GetComponentInParent<EntityData>();
                if (data != null && data != Origin)
                {
                    if (!hit.collider.CompareTag("NonDestroyable"))
                    {
                        data.CurrentHealth.Value -= Damage;
                        data.OnDamageTaken.OnNext(Damage);    
                    }
                }

                if (data == null || data != Origin)
                {
                    var point = hit.point;
                    point.y = transform.position.y;
                    Explode(point);
                    return;    
                }
            }
            
            transform.Translate(Vector3.forward * Time.deltaTime * Speed);
            _distanceTraveled += distance;
            if (_distanceTraveled > MaxDistance)
            {
                LeanPool.Despawn(this);
            }
        }

        private void Explode(Vector3 position)
        {
            var item = LeanPool.Spawn(OnCollisionParticlesPrefab, position, Quaternion.identity);
            var renderer = GetComponentInChildren<Renderer>();
            if (renderer != null)
            {
                var particles = item.GetComponent<ParticleSystem>();
                var main = particles.main;
                main.startColor = renderer.material.color;    
            }

            var audio = LeanPool.Spawn(CollisionAudioSource, position, Quaternion.identity);
            audio.Play();
            
            LeanPool.Despawn(audio, 1f);
            LeanPool.Despawn(item, 1f);
            LeanPool.Despawn(this);
        }
    }
}